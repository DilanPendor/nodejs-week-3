var express = require('express');
var router = express.Router();
var authController = require('../../controllers/api/authControllerAPI');

router.post('/authenticate',authController.authenticate);
router.post('/forgotPAssword',authController.forgetPassword);

module.exports = router;